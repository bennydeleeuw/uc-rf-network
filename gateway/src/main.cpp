#include "secrets.h"

#define deviceName "serial_mqtt"

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>


void setup_wifi();
void checkMqtt();
void serialEvent();
void notify(const char *topic, const char *payload);


const byte LED = 2;


const byte HC12RxdPin = 4; // "RXD" Pin on HC12
const byte HC12TxdPin = 5; // "TXD" Pin on HC12


const char *ssid = WIFI_SSID;
const char *password = WIFI_PASS;
const char *mqtt_server = MQTT_SERVER;

SoftwareSerial HC12(HC12TxdPin, HC12RxdPin); // Create Software Serial Port

WiFiClient espClient;
PubSubClient mqttClient(espClient);

bool ready = false;

String inputString = "";        // a string to hold incoming data
boolean stringComplete = false; // whether the string is complete

void setup()
{

    Serial.begin(9600); // Open serial port to computer
    HC12.begin(9600);   // Open serial port to HC12
    inputString.reserve(200);

    setup_wifi();

    mqttClient.setServer(mqtt_server, 1883);

    checkMqtt();

    pinMode(LED, OUTPUT);

    digitalWrite(LED, LOW);
    delay(1000);
    digitalWrite(LED, HIGH);
}

void checkMqtt()
{
    mqttClient.loop();

    if (mqttClient.connected())
        return;

    // Loop until we're reconnected
    while (!mqttClient.connected())
    {
        Serial.println("Attempting MQTT connection...");
        // Attempt to connect
        if (mqttClient.connect(deviceName))
        {
            Serial.println("MQTT Connected");
        }
        else
        {
            Serial.print("failed, rc=");
            Serial.print(mqttClient.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

void setup_wifi()
{

    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void loop()
{
    checkMqtt();
    serialEvent();
}

void serialEvent()
{

    if (HC12.available())
    { // If Arduino's HC12 rx buffer has data

        String data = HC12.readStringUntil('\n');
        // Serial.write(HC12.read()); // Send the data to the computer
        Serial.println(data);

        notify("hc12/message", data.c_str());

        digitalWrite(LED, LOW);
        delay(10);
        digitalWrite(LED, HIGH);
        delay(10);
    }

}

void notify(const char *topic, const char *payload)
{    
    mqttClient.publish(topic, payload);
}
 