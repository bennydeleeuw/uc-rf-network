
#define NAME "test"

#include <ArduinoJson.h>
#include <Arduino.h>

#include <SoftwareSerial.h>

const byte HC12RxdPin = 4; // "RXD" Pin on HC12
const byte HC12TxdPin = 5; // "TXD" Pin on HC12
const byte HC12SetPin = 6; // "SET" Pin on HC12
const byte LED = 13;

SoftwareSerial HC12(HC12TxdPin, HC12RxdPin); // Create Software Serial Port

int counter = 0;

StaticJsonDocument<200> doc;

void setup()
{
  HC12.begin(9600); // Open serial port to HC12
  pinMode(LED, OUTPUT);
}

void loop()
{
  doc["name"] = NAME;
  doc["counter"] = counter++;
  
  serializeJson(doc, HC12);
  
  digitalWrite(LED, HIGH);
  delay(100);
  digitalWrite(LED, LOW);
  delay(1000);
}

